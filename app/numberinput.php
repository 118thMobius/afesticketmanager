<?php
/*
  Create :2015/3/20 14:48
  Author A.Sato (@66thGALM MobiusOne.org)

  QRコードを読み込めなかったお客様がアクセスする。
  HTMLのFORM要素でnavigate.phpにnum=番号を飛ばすのみ。
  特別なことは何もない。input type="number"のmin max要素はHTML5かららしいので注意。
  HTML5非対応ブラウザからのアクセスが予想される場合は、Submitする際にJavaScriptでチェックをかけるのが合理的かと思われる。(本コードでは環境依存を最低限にしたいので省略。)
*/
  ?>
<!doctype html>
<!-- Author @66thGALM -->
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
  <title>番号を入力してください</title>
  <link rel="stylesheet" type="text/css" href="console.css">
</head>
<body>
  <div id="header-fixed">
    <div id="header">
      <div id="menu">
        <a href="">番号を入力</a>
      </div>
    </div>
  </div>

  <div id="body">
      <div id="loginform">
        <form id="loginForm" action="navigate.php" method="GET">
          <label for="userid">カードの番号を入力してください。</label><br>
          <!--maxに整理券の印刷枚数を入力。-->
          <input type="number" name="num" min="1" max="250" value="1">
          <BUTTON id="loginbutton" >入力</BUTTON>
        </form>
      </div>
  </div> 
</body>
</html>