<?php
/*
  Create :2015/3/20 13:40
  Author A.Sato (@66thGALM MobiusOne.org)

  お客様に開いていただくページ。
  ご案内文については利用状況に応じて編集のこと。
  「URL?num=番号」というURLにアクセスすることでそのお客様にナビゲーションを行う。
  ちゃんと上の方法でカード番号が指定されていないと、numberinput.phpにリダイレクトする。

  Ver0.2からリダイレクトをHTTPヘッダで行うようになったのでJavaScriptnに非依存になった。
*/
  ?>
  <!DOCTYPE html>
  <!-- Author @66thGALM -->
  <html>
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <meta http-equiv="Refresh" content="30">
    <title>ご案内</title>
    <link rel="stylesheet" type="text/css" href="console.css">
  </head>
  <body>
    <div id="header-fixed">
      <div id="header">
        <div id="menu">
          <a href="">ご案内</a>
        </div>
      </div>
    </div>

    <div id="body">
      <?php
      //発行するカードの枚数をここに入力して使用する。データベースは500件まで用意されている(拡張可能)。
      $cardNum = $_GET['num'];
      if (is_null($cardNum)) 
      {
        print('<font color="red">カード番号を指定してください。</font><br>リダイレクトしています。正しく行われない時は<a href="numberinput.php">ここ</a>をクリック。');
        //JavaScriptが利用されていた部分。
        //print('<script type="text/javascript"><!--'."\n".'location.href="numberinput.php";'."\n".'//--></script>');
        header("Location: numberinput.php");
      }
      else
      {        
        try
        {
          //データベースをオープン
          //PDO(PDO('mysql:host=localhost;dbname=データベース名;charset=utf8','ユーザー名','パスワード'))で引数を渡す。
          $db = new PDO('mysql:host=localhost;dbname=btm_develop;charset=utf8','user','password');
          $stt=$db->query('SELECT * FROM cardlist');
          print('このページは30秒毎に更新されます。<br><br>');
          //データベースを切断。
          $db= NULL;

          $waitList = array();
          $received;

          $i = 0;
          while ($row = $stt->fetch(PDO::FETCH_OBJ))
          {
            $i++;
            switch($row->state) 
            {
              case 'wait':
                if($i<$cardNum)
                {
                  $waitList[] = $row->number;
                }
                break;
              case 'receive':
                if ($i>=$cardNum) 
                {
                  $received = true;
                }
                break;
              default:
                break;
            }
          }
          if ($received) {
            //すでにご案内していたらここが出力される。
            print('すでにご案内しています。受付までお早めにお越しください。');
          }
          else
          {
            if (count($waitList)==0){
              //次に呼ばれるならここが出力される。
                print('次にお呼びします。余裕を持って受付までお越しください。');
            }
            else
            {
              if (count($waitList)<5)
              {
              //残り5人未満ならここが出力される。
                print('まもなくご案内いたします。');
                print('<br>あと').count($waitList).('名です。');
              }
              else
              {
              //残り5人以上ならここ
                print('順番にご案内しています。今しばらくお待ちください。');

              }
            }
          }
        }
        catch(PDOException $e)
        {
          die("エラーが発生しました。".$e->getMessage());
        }
      }
      ?>
    </div> 
  </body>
  </html>