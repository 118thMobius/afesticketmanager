<?php
/*
  Create :2015/3/14 22:56
  Author A.Sato (@66thGALM MobiusOne.org)

  管理用のページ。
  基本的には受付ではここを開いているはず。
  オレンジが案内待ち
  水色が案内済み
  黄色はデータベースのデータにエラーが発生していることを示す。

  案内待ちのボタンは押すと案内済みに
  案内済みのボタンは押すと案内待ちに
  エラー表示のボタンは押すと案内待ちにセットされる。
  右上の歯車から設定画面に。

  処理はクエリ系のページに飛んで行うので、このページでは表示を行っているだけ。
  30秒ごとに再読込される。

  コード中にもコメントアウトしてあるが、設定でカードを何枚まで処理するのかを設定する。
*/


session_start();
// ログイン状態のチェック
if (!isset($_SESSION["USERID"])) {
  header("Location: logout.php");
  exit;
}
?>
<!DOCTYPE html>
<!-- Author @66thGALM -->
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
  <meta http-equiv="Refresh" content="30">
  <title>管理コンソール</title>
  <link rel="stylesheet" type="text/css" href="console.css">
</head>
<body>
  <div id="header-fixed">
    <div id="header">
      <div id="menu">
        <a href="">管理コンソール</a>
      </div>
      <div id="setting">
        <a href="setting.php"><img src="images/ic_settings_white_48dp.png"></a>
      </div>
    </div>
  </div>

  <div id="body">
    <?php
    //発行するカードの枚数をここに入力して使用する。データベースは500件まで用意されている(拡張可能)。
    $maxCardNum = 250;
    try
    {
    //データベースをオープン
    //PDO(PDO('mysql:host=localhost;dbname=データベース名;charset=utf8','ユーザー名','パスワード'))で引数を渡す。
      $db = new PDO('mysql:host=localhost;dbname=btm_develop;charset=utf8','user','password');
      $stt=$db->query('SELECT * FROM cardlist');
      print('データベース接続成功('.date("Y年m月d日 Ag:i:s").')。このページは30秒毎に更新されます。<br>');
    //データベースを切断。
      $db= NULL;

      $waitList = array();
      $receivedList = array();

      $i = 0;
      while ($row = $stt->fetch(PDO::FETCH_OBJ))
      {
        $i++;
        if ($i > $maxCardNum) {
          break;
        }
        switch($row->state) {
          case 'wait':
            print('<form action="stateupdate.php" method="POST" style="display: inline">');
            print('<input type="hidden" name="number" value="'.$row->number.'">');
            print('<input type="hidden" name="state" value="receive">');
            print('<BUTTON class="wait" title="案内待ち">'.$row->number.'</BUTTON>');
            print('</form>');
            $waitList[] = $row->number;
            break;
          case 'receive':
            print('<form action="stateupdate.php" method="POST" style="display: inline">');
            print('<input type="hidden" name="number" value="'.$row->number.'">');
            print('<input type="hidden" name="state" value="wait">');
            print('<BUTTON class="receive" title="案内済み">'.$row->number.'</BUTTON>');
            print('</form>');
            $receivedList[] = $row->number;
            break;
          default:
            print('<form action="stateupdate.php" method="POST" style="display: inline">');
            print('<input type="hidden" name="number" value="'.$row->number.'">');
            print('<input type="hidden" name="state" value="wait">');
            print('<BUTTON class="error" title="データベース上に問題が発生しています。">'.$row->number.'</BUTTON>');
            print('</form>');
            break;
        }
      }
    }
    catch(PDOException $e)
    {
      die("エラーが発生しました".$e->getMessage());
    }
    ?>
  </div> 

  <div id="footer-fixed">
    <div id="footer">
      <div id="menu">
        ↓次にお呼びするお客様 (ご案内済み人数
      	<?php
      	print(count($receivedList)); 
      	?>)
        <br>
        <?php
        $i = 0;
        $text;
        foreach($waitList as $w)
        {
          $text = $text.$w.'> ';
          if (strlen($text) > 178) {
            break;
          }
        }
        print($text);
        ?>
      </div>
    </div>
  </div>
</body>
</html>