<?php
/*
  Create :2015/3/16 22:10
  Author A.Sato (@66thGALM MobiusOne.org)

  ステータスをアップデートする。
  POSTでnumberとstateを投げるとPDOのUPDATEメソッドを実行する。
  正常に処理されればコールしたページにリダイレクトする。
*/
session_start();
// ログイン状態のチェック
if (!isset($_SESSION["USERID"])) {
  header("Location: logout.php");
  exit;
}
?>
<!DOCTYPE html>
<!-- Author @66thGALM -->
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
  <title>更新中</title>
  <link rel="stylesheet" type="text/css" href="console.css">
</head>
<body>
  <div id="header-fixed">
    <div id="header">
      <div id="menu">
        <a href="">更新中………</a>
      </div>
    </div>
  </div>

  <div id="body">
    <?php
    try
    {
    //データベースをオープン
    //PDO(PDO('mysql:host=localhost;dbname=データベース名;charset=utf8','ユーザー名','パスワード'))で引数を渡す。
      $db = new PDO('mysql:host=localhost;dbname=btm_develop;charset=utf8','user','password');
      print('データベース接続成功('.date("Y年m月d日 Ag:i:s").')<br>');
      $stt=$db->prepare('UPDATE cardlist SET state=:state WHERE number=:number');
      $stt->bindValue(':state',$_POST['state']);
      $stt->bindValue(':number',$_POST['number']);
      $stt->execute();
      //データベースを切断。
      $db= NULL;
      print($_POST['number'].'番をステータス'.$_POST['state'].'に更新に成功。<br>リダイレクトしています。正しく行われない時は<a href="'.$_SERVER["HTTP_REFERER"].'">ここ</a>をクリック。');
      header("Location: ".$_SERVER["HTTP_REFERER"]);
    }
    catch(PDOException $e)
    {
      die("エラーが発生しました".$e->getMessage()."<br>前のページに戻るには<a href=".$_SERVER["HTTP_REFERER"].">ここ</a>"."をクリック。");
    }
    ?>
  </div> 
</body>
</html>