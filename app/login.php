<?php
/*
  Create :2015/3/16 23:32
  Author A.Sato (@66thGALM MobiusOne.org)

  管理用ページのログアウト画面。
  認証されたセッションを作成する。
  コンソール系はここを経てアクセスしないと操作を弾かれる。

  認証はPHPのセッションを利用するだけの単純な認証(通信は平文)なので注意。
  この認証方法について詳しくは http://d.hatena.ne.jp/replication/20100828/1282994791 を参照。
  要するにLoginボタンを押すと自分にフォームの内容を送信して正しければ新しいセッションを発行する。
  セッションから$_POST["userid"]でログインに使ったIDが取得できるが、 使い道はあまりないと思われる。

  サイトではPHPの$_SERVER['PHP_SELF']を利用しているが、あまり安全性のある方法とは言えないので空にしておく。
  金銭的損害が発生するようなデータでもないのでこのまま(平文通信)でも問題はないと思われるが、暇があればちゃんとした暗号化を施すに越したことはない。



  あとここでやる気が途絶えたので一番デザイン(というかCSS)が雑。
*/
  session_start();
  
  // メッセージ
  $errorMessage = "ログインしてください";
  // 画面に表示するため特殊文字をエスケープする
  $viewUserId = htmlspecialchars($_POST["userid"], ENT_QUOTES);

  // ログインボタンが押された場合      
  if (isset($_POST["login"])) {

    // 認証
    // ここのhoge２つをユーザーIDとパスワードに書き換えてください。
    if ($_POST["userid"] == "hoge" && $_POST["password"] == "hoge") {
      // セッションIDを新規に発行する
      session_regenerate_id(TRUE);
      $_SESSION["USERID"] = $_POST["userid"];
      header("Location: console.php");
      exit;
    }
    else {
      $errorMessage = '<font color="red">ユーザIDあるいはパスワードに誤りがあります。</font>';
    }
  }

?>
<!doctype html>
<!-- Author @66thGALM -->
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
  <title>ログイン</title>
  <link rel="stylesheet" type="text/css" href="console.css">
</head>
<body>
  <div id="header-fixed">
    <div id="header">
      <div id="menu">
        <a href="">ログイン</a>
      </div>
    </div>
  </div>

  <div id="body">
    <?php
      print ($errorMessage);
      ?>
      <div id="loginform">
        <form id="loginForm" name="loginForm" action="" method="POST">
          <label for="userid">ユーザID</label><br>
          <input type="text" id="userid" name="userid" value="<?php echo $viewUserId ?>">
          <br>
          <label for="password">パスワード</label><br>
          <input type="password" id="password" name="password" value="">
          <br>
          <BUTTON id="loginbutton" name="login">ログイン</BUTTON>
        </form>
      </div>
  </div> 

</body>
</html>