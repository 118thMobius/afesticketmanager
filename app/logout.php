<?php
/*
  Create :2015/3/16 23:32
  Author A.Sato (@66thGALM MobiusOne.org)

  管理用ページのログアウト画面。
  セッションを破棄する。
  注意事項についてはlogin.phpを参照のこと。
*/
session_start();

if (isset($_SESSION["USERID"])) {
  $errorMessage = "ログアウトしました。";
}
else {
  $errorMessage = "セッションがタイムアウトしたか、不正な経路での接続です。";
}
// セッション変数のクリア
$_SESSION = array();
// クッキーの破棄
if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}
// セッションクリア
@session_destroy();
?>

<!DOCTYPE html>
<!-- Author @66thGALM -->
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
  <title>ログアウト</title>
  <link rel="stylesheet" type="text/css" href="console.css">
</head>
<body>
  <div id="header-fixed">
    <div id="header">
      <div id="menu">
        <a href="">ログアウト</a>
      </div>
    </div>
  </div>

  <div id="body">
    <?php echo $errorMessage; ?>
    <br><a href="login.php">ログイン画面に戻る</a>
  </div> 
</body>
</html>