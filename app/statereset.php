<?php
/*
  Create :2015/3/16 23:08
  Author A.Sato (@66thGALM MobiusOne.org)

  ステータスをすべてリセットする。
  この操作は取り消せないので注意すること。
  一応の安全措置としてPOSTで「$exec=true」を渡さないと実行されない。
*/
session_start();
// ログイン状態のチェック
if (!isset($_SESSION["USERID"])) {
  header("Location: logout.php");
  exit;
}
?>
<!DOCTYPE html>
<!-- Author @66thGALM -->
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
  <title>リセット中</title>
  <link rel="stylesheet" type="text/css" href="console.css">
</head>
<body>
  <div id="header-fixed">
    <div id="header">
      <div id="menu">
        <a href="">リセット中………</a>
      </div>
    </div>
  </div>

  <div id="body">
    <?php
    if($_POST['exec'])
    {
      try
      {
        //データベースをオープン
        //PDO(PDO('mysql:host=localhost;dbname=データベース名;charset=utf8','ユーザー名','パスワード'))で引数を渡す。
        $db = new PDO('mysql:host=localhost;dbname=btm_develop;charset=utf8','user','password');
        print('データベース接続成功('.date("Y年m月d日 Ag:i:s").')<br>');
        $stt=$db->query('UPDATE cardlist SET state="wait" WHERE 1');
        //データベースを切断。
        $db= NULL;
        print('ステータスのリセットに成功。<br>元のページに戻るには<a href="'.$_SERVER["HTTP_REFERER"].'">ここ</a>をクリック。');
        //リダイレクトするとリセットされたことがわかりにくいので修正。
        //場合によっては有効に
        //header("Location: ".$_SERVER["HTTP_REFERER"]);
      }
      catch(PDOException $e)
      {
        die("エラーが発生しました".$e->getMessage());
      }
    }
    else
    {
      print('<font color="red">!実行許可が与えられていないため、ステータスのリセットは行われませんでした。</font><br>前のページに戻るには<a href="'.$_SERVER["HTTP_REFERER"].'">ここ</a>をクリック。');
    }
    ?>
  </div> 
</body>
</html>