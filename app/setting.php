<?php
/*
  Create :2015/3/16 23:56
  Author A.Sato (@66thGALM MobiusOne.org)

  各種設定を行う。
  右上のアイコンからコンソールをログアウトできる。
  データベースのリセットは行う前に確認ダイアログが表示される。
*/
session_start();
// ログイン状態のチェック
if (!isset($_SESSION["USERID"])) {
  header("Location: logout.php");
  exit;
}
?>
<!DOCTYPE html>
<!-- Author @66thGALM -->
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
  <title>設定</title>
  <link rel="stylesheet" type="text/css" href="console.css">

<script type="text/javascript"> 
<!-- 

  function checksubmit()
  {
   // リセットを行うか確認ダイアログを表示
    if(window.confirm('本当にデータベースをリセットしますか？(この操作は元に戻せません)'))
    {
     document.resetform.submit();
    }
    else
    {
     return false; // 送信を中止
    }
  }
  // -->
</script>
</head>
<body>
  <div id="header-fixed">
    <div id="header">
      <div id="menu">
        <a href="">設定</a>
      </div>
      <div id="setting">
        <a href="logout.php"><img src="images/ic_exit_to_app_white_48dp.png"></a>
      </div>
    </div>
  </div>

  <div id="body">
    <a href="console.php">コンソールに戻る。</a>
    <form id="resetform" name="resetform" action="statereset.php" method="POST">
    <input type="hidden" name="exec" value="true">
    <a href="javascript:checksubmit()">データベースをリセットする。</a>
    </form>
  </div> 
</body>
</html>